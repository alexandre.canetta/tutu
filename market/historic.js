/* eslint-disable default-param-last */

const publicCall = require('../api/publicCall');

async function get(symbol, interval, startTime, endTime) {
  return await publicCall.klines(symbol, interval, startTime, endTime);
}

module.exports = { get };
