// const defaultDecimal = process.env.DEFAULT_DECIMAL;
const defaultDecimal = 8;

function movingAverage(historic = {}, periods = []) {
    let total;
    
    periods = periods.sort((low, high) =>  low[0] - high[0]);
    periods.map(period => {        
        for (i = 0; i < Object.keys(historic).length; i++) {            
            total = 0;
            if (Object.values(historic)[i - period + 1]  != undefined) {
                if (Object.values(historic)[i][period] == undefined) {
                    for (let j = i;  j > i - period; j--) { 
                        total = Number((total + Object.values(historic)[j]['price'])); 
                    }
                } 
            }
            historic[Object.keys(historic)[i]][period] = Number((total / period).toFixed(defaultDecimal));       
        }
    });   
    // for (const key in historic) {
    //     delete historic[key]['price'];  
    //     if (historic[key][periods[periods.length - 1]] == 0) {
    //         delete historic[key];
    //     }
    // };
    
    return historic;
}

module.exports = { movingAverage }