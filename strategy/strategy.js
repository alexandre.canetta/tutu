const movingAverageCross = require('./movingAverageCross');
const trade = require('./trade');

module.exports = { movingAverageCross, trade };
