const technicalAnalysis = require('../technicalAnalysis/technicalAnalysis');
const tools = require('../tools/tools');
// const statistics = require('../statistics/statistics');

async function get(data = {}, flgPosition, priceType, periods) {     
    let sctHistoric = {};
    let uptHistoric = {};
    let maxMovingAveragePeriods = periods[periods.length - 1];

    sctHistoric = tools.object.regressive(data['base'], maxMovingAveragePeriods - 1);
    sctHistoric = { ...sctHistoric, ...data['update'] };

    for (const key in sctHistoric) {
        uptHistoric[key] = { 
            priceType : priceType, 
            price : sctHistoric[key][priceType]                
        };
    }
    
    uptHistoric = technicalAnalysis.trendline.movingAverage(uptHistoric, periods);
    
    for (const key in uptHistoric) {
        // console.log(key)
        sctHistoric[key]['movingAverage'] = { ...uptHistoric[key] };
    }

    // for (const key in sctHistoric) {
    //     if (sctHistoric[key]['movingAverage'] != undefined) {
    //         uptHistoric[key] = sctHistoric[key]['movingAverage'];
    //     }
    // }

    uptHistoric = {};

    if (Object.keys(data['base']).length > 0) {
        uptHistoric[Object.keys(data['base']).pop()] = data['base'][Object.keys(data['base']).pop()]['movingAverage'];
    }

    for (const key in data['update']) {
        if (data['update'][key]['movingAverage'] != undefined) {
            uptHistoric[key] = data['update'][key]['movingAverage'];
        }
    }

    uptHistoric = tools.technicalAnalysis.crossValues(uptHistoric);

    for (const key in uptHistoric) {
        sctHistoric[key]['movingAverage']['crossValues'] = { ...uptHistoric[key] };
    }     

    for (const key in sctHistoric) {
        if (sctHistoric[key]['movingAverage'] != undefined) {
            uptHistoric[key] = sctHistoric[key]['movingAverage']['crossValues'];
        }
    } 

    uptHistoric = tools.technicalAnalysis.crossOrder(uptHistoric, flgPosition, 'movingAverageCross');

    for (const key in uptHistoric) {
        if (sctHistoric[key]['strategy'] == undefined) {
            sctHistoric[key]['strategy'] = {};
        }
        sctHistoric[key]['strategy']['movingAverageCross'] = uptHistoric[key];        
    };

    // console.log('uptHistoric')
    // console.log(uptHistoric)
    // for (const key in sctHistoric) {        
    //     if (sctHistoric[key]['strategy'] != undefined) {
    //         if (sctHistoric[key]['strategy']['movingAverageCross'] != undefined) {
    //             uptHistoric[key] = {
    //                 side : sctHistoric[key]['strategy']['movingAverageCross'],
    //                 price : sctHistoric[key]['open']
    //             };
    //         }
    //     }
    // }
    //############### IMPRIME ORDENS #############
    // for (const key in uptHistoric) {
    //     if (uptHistoric[key]['order'] != '') {
    //         console.log(key, uptHistoric[key]);
    //     }
    // };
    //############### IMPRIME ORDENS #############
    // uptHistoric = statistics.profit.get(uptHistoric);
    // console.log(await sctHistoric)
    return sctHistoric;
}

module.exports = { get };