const { remote, ipcRenderer } = require('electron')


const Historic = require('./market/historic.js');
const strategy = require('./strategy/strategy');
const publicCall = require('./api/publicCall.js');
const tools = require('./tools/tools');

let btnNew =  document.getElementById("btnNew");
btnNew ? btnNew.addEventListener("click", goToNew) : null;

let btnRefresh =  document.getElementById("btnRefresh");
btnRefresh ? btnRefresh.addEventListener("click", refresh) : null;
// btnVar ? btnVar.addEventListener("click", getSymbol()) : null;

let divRoot =  document.getElementById("root");
// divRoot ?  divRoot.innerHTML = analysis : null

let divBottom =  document.getElementById("bottom");
divBottom ? divBottom.addEventListener("load", testVar()) : null
// divBottom ? divBottom.addEventListener("load", getSymbol()) : null

let selectSymbol =  document.getElementById("symbol");
selectSymbol ? getSymbol() : null;

// analysisId > 0 ?  divRoot.innerHTML = analysis : null
// divRoot.innerHTML = analysis;
// divRoot && analysisId > 0 ? divRoot.addEventListener("click", testVar) : null;

    // ipcRenderer.send('getInner');
    // ipcRenderer.on('clipboard-inner', (event, json) => {
    //     console.log(json);
    //     // console.log(divRoot)
    //     divRoot.innerHTML = json;
    // }); 

// let divRootNew =  document.getElementById("rootNew");

let frmNew =  document.getElementById("frmNew");
frmNew ? frmNew.addEventListener("submit", getNew) : null;

function goToNew(){      
    ipcRenderer.send(
        'openChildWindow', 
        {
            file: 'new',
            width: 470,
            height: 340
        }
    );  
}

async function refresh(event) {
    event.preventDefault();
    ipcRenderer.send('getInner');
    ipcRenderer.on('inner', (event, json) => {
        divBottom.innerHTML = json;
    }); 
}

async function getSymbol(){
    let exchangeInfo = await publicCall.exchangeInfo();
    let inner = 
        `<option disabled selected value></option>`
    ;
    for (var i = 0; i < exchangeInfo.symbols.length; i++) {
        inner +=
            `<option value=${exchangeInfo.symbols[i].symbol}>` +
                exchangeInfo.symbols[i].symbol +
            `</option>`
        ;
    }
    selectSymbol.innerHTML = inner;
}

async function getNew(event) {    
    event.preventDefault();

    let params = {
        symbol: event.target.elements.symbol.value,
        strategy: event.target.elements.strategy.value,
        startTime: event.target.elements.startTime.value,
        endTime: event.target.elements.endTime.value,
        interval: event.target.elements.interval.value,
        shortLine: event.target.elements.shortLine.value,
        longLine: event.target.elements.longLine.value
    };

    let analysis = await calculateStrategy(params);    
    // analysis = await calculateStrategy(params);    
    ipcRenderer.send('setAnalysis', analysis);
    ipcRenderer.send('getAnalysis');    
    ipcRenderer.on('analysis', async (event, value) => {
        ipcRenderer.send('setInner', await mount(await value));
        window.close();
        // ipcRenderer.send('refresh');
    });

    // window.close();

    // ipcRenderer.send('refresh');
}

async function calculateStrategy(params) {
    let flgPosition = false;
    let historic = {};
    let movingAveragePeriods = [ parseInt(params.shortLine), (parseInt(params.shortLine) + 1) , parseInt(params.longLine) ];

    historic['base'] = {}
    historic['update'] = await Historic.get(
        params.symbol, 
        params.interval, 
        params.startTime,
        params.endTime
    );
 
    let trades = await strategy.trade.get(
        await strategy.movingAverageCross.get(historic, flgPosition, 'close', movingAveragePeriods)
    );
    let resume = await strategy.trade.resume(trades);
    
    return {
        params,
        trades,
        resume
    };  
}

async function mount(cards) {
    let inner = 
        `<div class=carousel>`     
            // `<div class=nav nav-left>` +
            //     `<button type="button"><</button>` +
            // `</div>` +
            // `<div class=carousel-content>`
    ;
    for (const key in cards) {        
        // console.log(new Date(cards[key]['params']['startTime']).toLocaleString('pt-BR'))
        inner += 
        // `<div id=${key} class=slide>` +
        `<div id=${key} class=card>` +
            `<table>` +
                `<tr>` +
                    `<td id="symbol" colspan="3">` + 
                        cards[key]['params']['symbol'] + 
                    `</td>` + 
                `</tr>` +

                `<tr>` +
                    `<td id="strategy" colspan="3">` + 
                        // cards[key]['params']['strategy'] +
                        `Cruzamento de Médias Móveis` +
                    `</td>` +    
                `</tr>` +

                `<tr>` +
                    `<td id="period" colspan="3">` +
                        (new Date(cards[key]['params']['startTime'])).toLocaleString('pt-BR') +  ` - ` + (new Date(cards[key]['params']['endTime'])).toLocaleString('pt-BR') +
                    `</td>` + 
                `</tr>` +

                `<tr>` +
                    `<td id="interval" colspan="3">` +
                        cards[key]['params']['interval'] + ` - ` +  `[` + cards[key]['params']['shortLine'] + `, ` + cards[key]['params']['longLine'] + `]` +
                    `</td>` +   
                `</tr>` +

                `<tr>` +
                    `<td id="line" colspan="3">` +
                    ` ` +
                    `</td>` + 
                `</tr>` + 

                `<tr>` +
                    `<td class="tableTitle">` +
                        `Tipo` +
                    `</td>` + 
                    `<td class="tableTitle">` +
                        `Quantidade` +
                    `</td>` + 
                    `<td class="tableTitle">` +
                        `Valor` +
                    `</td>` + 
                `</tr>` +


                `<tr>` +
                    `<td class="tableBody">` +
                        `Ganhos    ` +
                    `</td>` + 
                    // `<td class="tableTitle" v-maska="['##.###,##']">` +
                    `<td class="tableBody">` +
                        // ("0000" + cards[key]['resume']['winQty']).slice(-4) +
                        cards[key]['resume']['winQty'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + 
                    `</td>` +                 
                    `<td class="tableBody">` +
                        // ("000000" + cards[key]['resume']['winVal']).slice(-6) + 
                        // cards[key]['resume']['winVal'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + 
                        cards[key]['resume']['winVal'] + 
                    `</td>` + 
                `</tr>` +

                `<tr>` +
                    `<td class="tableBody">` +
                        `Perdas    ` +
                    `</td>` + 
                    `<td class="tableBody">` +
                        // ("0000" + cards[key]['resume']['loseQty']).slice(-4) +
                        cards[key]['resume']['loseQty'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + 
                    `</td>` +                 
                    `<td class="tableBody">` +
                        // ("000000" + cards[key]['resume']['loseVal']).slice(-6) + 
                        // cards[key]['resume']['loseVal'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + 
                        cards[key]['resume']['loseVal'] +
                    `</td>` + 
                `</tr>` +

                `<tr>` +
                    `<td class="tableBody">` +
                        `Empates   ` +
                    `</td>` + 
                    `<td class="tableBody">` +
                        // ("0000" + cards[key]['resume']['drawQty']).slice(-4) +
                        cards[key]['resume']['drawQty'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + 
                    `</td>` +                 
                    `<td class="tableBody">` +
                        // ("000000" + cards[key]['resume']['drawVal']).slice(-6) + 
                        // cards[key]['resume']['drawVal'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + 
                        cards[key]['resume']['drawVal'] + 
                    `</td>` + 
                `</tr>` +

                `<tr>` +
                    `<td class="tableBody">` +
                        `Total    ` +
                    `</td>` +  
                    `<td class="tableBody">` +
                        // ("0000" + cards[key]['resume']['trades']).slice(-4) +
                        cards[key]['resume']['trades'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + 
                    `</td>` +                
                    `<td class="tableBody">` +
                    // ("000000" + cards[key]['resume']['absolute']).slice(-6) +
                    // cards[key]['resume']['absolute'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + 
                        cards[key]['resume']['absolute'] + 
                    `</td>` + 
                `</tr>` +

                // `<tr>` +
                //     `<td>` +
                //     `</td>` + 
                //     `<td>` +
                //         `<button type="button">` + 
                //             `Detalhamento` + 
                //         `</button>` +
                //     `</td>` + 
                //     `<td>` +
                //     `</td>` + 
                // `</tr>` +
            `</table>` +
        `</div>`
        ;
    }
    inner += 
        // `</div>` +
        //     `<div id=button class=nav nav-right>` +
        //         `<button id=next type="button">></button>` +
        //     `</div>` +
        `</div>`
    ;
    return inner;
}