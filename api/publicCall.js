/* eslint-disable import/prefer-default-export */
/* eslint-disable prettier/prettier */

// import { publicCall } from './call';
const publicCall = require('./call').publicCall;

const tools = require('../tools/tools');

// const defaultSymbol = process.env.DEFAULT_SYMBOL;
// const defaultInterval = process.env.DEFAULT_INTERVAL;
// const defaultLimit = process.env.DEFAULT_LIMIT;
// const defaultStartTime = process.env.DEFAULT_STARTTIME;
// CRAWLER_INTERVAL=3000
// PROFITABILITY= 1.05
// DEFAULT_SYMBOL=BTCBRL
// DEFAULT_INTERVAL=1d
// DEFAULT_LIMIT=5
// #30/11/2022
// DEFAULT_STARTTIME=1669849200000
// #VERFICIAR CASAS DECIMAIS DE OUTRAS MOEDAS / PAR (8 É SHIBBRL)
// DEFAULT_DECIMAL=8

const defaultSymbol = "BTCBRL";
const defaultInterval = "1d";
const defaultLimit = 5;
const defaultStartTime = 1669849200000;


// export function time() {
function time() {
  return publicCall('/v3/time');
}

// export async function depth(symbol = defaultSymbol, limit = defaultLimit) {
async function depth(symbol = defaultSymbol, limit = defaultLimit) {
  return publicCall('/v3/depth', { symbol, limit });
}

// ARRUMAR PARAMETROS- NOME DA FUNCAO???
// export async function statistcs(symbol = defaultSymbol /* , openTime = 1499783499040, closeTime = 1499869899040 */){
async function statistcs(symbol = defaultSymbol /* , openTime = 1499783499040, closeTime = 1499869899040 */){
  return publicCall('/v3/ticker/24hr', { symbol /* , openTime, closeTime */ });
}

// export async function exchangeInfo(symbol = defaultSymbol /* , limit = defaultLimit */) {
// async function exchangeInfo(symbol = defaultSymbol /* , limit = defaultLimit */) {
async function exchangeInfo(symbol /* , limit = defaultLimit */) {
  return await publicCall('/v3/exchangeInfo');
}

// export async function trades(symbol = defaultSymbol, limit = 500 /* max 1000 */) {
async function trades(symbol = defaultSymbol, limit = 500 /* max 1000 */) {
  return publicCall('/v3/trades', { symbol, limit });
}

// export async function price(symbol = defaultSymbol) {
async function price(symbol = defaultSymbol) {
  return publicCall('/v3/ticker/price', { symbol });
}

// VERIFICAR LIMIT POR REQUISICAO - NAO FUNCIONA
// export async function klines(symbol = defaultSymbol, interval = defaultInterval, startTime = defaultStartTime, endTime = Number(new Date()) /* , limit = 'max' */){
async function klines(symbol = defaultSymbol, interval = defaultInterval, startTime = defaultStartTime, endTime = Number(new Date()) /* , limit = 'max' */){
  // async function klines(symbol = defaultSymbol, interval = defaultInterval, startTime = defaultStartTime, endTime /* , limit = 'max' */){
  // interval  1m, 3m, 5m, 15m, 30m, 1h, 2h, 4h, 6h, 8h, 12h, 1d, 3d, 1w, 1mo
  const historic = {};

  startTime = tools.general.time(startTime);
  endTime = tools.general.time(endTime);  

  // console.log('hist2')
  // console.log('hist2')
  do {
    const response = await publicCall('/v3/klines', { symbol , interval , startTime , endTime });
    response.map(item => {
      historic[item[0]] = {
        startTime: item[0], // Kline open time
        open: parseFloat(item[1]), // Open price
        high: parseFloat(item[2]), // High price
        low: parseFloat(item[3]), // Low price
        close: parseFloat(item[4]), // Close price
        volume: parseFloat(item[5]), // Volume
        endTime: parseInt(item[6], 10), // Kline Close time
        volumeQuote: parseFloat(item[7]), // Quote asset volume
        trades: parseInt(item[8], 10), // Number of trades
        takerBuyBase: parseFloat(item[9]), // Taker buy base asset volume
        takerBuyQuote: parseFloat(item[10]), // Tsaker buy quote asset volume
        zero: parseInt(item[11], 10), // Unused field, ignore
        indicator: {},
        strategy: {},
        side: {},
      };
    });
    if (startTime === historic[Object.keys(historic).pop()]['startTime']) {
      break;
    }
    startTime = response[Object.keys(response).pop()][0];
  } while (true);
  // console.log(historic);
  return historic;
}

// ROTAS INTERESSANTES
// - GET /api/v3/ticker
// - GET /api/v3/ticker/bookTicker
module.exports = { time, depth, statistcs, klines, exchangeInfo, trades, price };
