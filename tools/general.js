// eslint-disable-next-line consistent-return
function time(date) {
  // if (date !== undefined) {
  //   return date.toString().length === 13 && date.toString().includes('-')
  //     ? new Date(date.replace(/[-]/g, '/')).getTime()
  //     : date;
  // }

  const datea = Date.parse(date); 
  // console.log(datea / 1000); 
  // console.log(datea); 
  return datea;

}

function milisecond(intervalString) {
  let intervalNumber;
  switch (intervalString) {
    case '1s':
      intervalNumber = 1000;
      break;
    case '1m':
      intervalNumber = 60000;
      break;
    case '3m':
      intervalNumber = 180000;
      break;
    case '5m':
      intervalNumber = 300000;
      break;
    case '15m':
      intervalNumber = 900000;
      break;
    case '30m':
      intervalNumber = 1800000;
      break;
    case '1h':
      intervalNumber = 3600000;
      break;
    case '2h':
      intervalNumber = 7200000;
      break;
    case '4h':
      intervalNumber = 14400000;
      break;
    case '6h':
      intervalNumber = 21600000;
      break;
    case '8h':
      intervalNumber = 28800000;
      break;
    case '12h':
      intervalNumber = 43200000;
      break;
    case '1d':
      intervalNumber = 86400000;
      break;
    case '3d':
      intervalNumber = 259200000;
      break;
    case '1w':
      intervalNumber = 604800000;
      break;
    case '1M':
      intervalNumber = 2678400000;
      break;
    default:
      intervalNumber = 86400000;
  }
  return intervalNumber;
}

module.exports = { time, milisecond };
