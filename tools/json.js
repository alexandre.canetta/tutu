const fs = require('fs');

async function write (file, text) {
    fs.writeFileSync(
        file, 
        JSON.stringify(text) , 
        'utf-8'
    )
        // function (err) {
        //     if (err) {
        //         console.log(err)
        //     }
        // }    
}

function read (file) {
    fs.readFile (
        file,
        function (err, data) {
            if (err) {
                console.log(err)
            }
            return JSON.parse(data);
        }
    )
}

function update (file, text){
    // console.log(text)
    let json = {};
    // fs.readFileSync(file, function (err, data) {
    fs.readFile(file, function (err, data) {
            console.log(data)
            if (data/*.toString()*/) {
                json = JSON.parse(data);
                json = {...json, ...text};
            } else {
                json = {...text}
            }
            write(file, json)   
    })
}

module.exports = { write, read, update }