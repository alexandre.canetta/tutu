// const json = require('./json');
const technicalAnalysis = require('./technicalAnalysis');
const object = require('./object');
const general = require('./general');

// module.exports = { json, technicalAnalysis, object, general };
module.exports = { general, object, technicalAnalysis };