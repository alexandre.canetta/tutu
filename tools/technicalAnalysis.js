function crossValues(historic = {}) {    
    for (i = 0 ; i < Object.keys(historic).length; i++) {              
        let period = Object.keys(historic)[i];
        let previousPeriod = i == 0 ? -1 : Object.keys(historic)[i - 1];
        if (historic[period]['crossValues'] == undefined) {
            for (j = 0; j < Object.keys(historic[period]).length; j++) {
                for (k = j + 1; k < Object.keys(historic[period]).length; k++) {
                    let high = Object.keys(historic[period])[j];
                    let low = Object.keys(historic[period])[k];
                    let highValue = historic[period][high];
                    let lowValue = historic[period][low];
                    let highXLow = high + 'x' + low;
                    let positionHigh;
                    let positionLow;
                    let cross = false;

                    if (isNaN(low)) {
                        break;
                    }
                    switch (true) {
                        case highValue > lowValue :
                            positionHigh = 'up';
                            positionLow = 'down';
                            break;
                        case highValue < lowValue :
                            positionHigh = 'down';
                            positionLow = 'up';
                            break;
                        default:
                            if (previousPeriod != -1) {
                                positionHigh = historic[previousPeriod]['crossValues'][highXLow]['high'];
                                positionLow =  historic[previousPeriod]['crossValues'][highXLow]['low'];
                            }
                            break;
                    }
                    if (historic[period]['crossValues'] == undefined) {
                        historic[period]['crossValues'] = {};
                    }
                    if (previousPeriod != -1) {
                        if (historic[previousPeriod]['crossValues'][highXLow]['high'] != positionHigh
                        ) {
                            cross = true;
                        } else {
                            cross = false;
                        }
                    }
                    
                    if (!positionHigh/* && previousPeriod != -1*/) {
                        // console.log(Object.keys(historic).length)
                        // console.log('a', previousPeriod)
                    }
                    
                    historic[period]['crossValues'][highXLow] = {
                        high : positionHigh,
                        low : positionLow,
                        cross : cross
                    };
                }
            }
        }
    }
    for (key in historic) {
        historic[key] = historic[key]['crossValues'];        
    }
    return historic;
}

function crossOrder(historic = {}, flgPosicao = false, strategy) {
    for (i = 0; i < Object.keys(historic).length; i++) {
        let period = Object.keys(historic)[i];
        let high = Object.values(historic[period])[0];
        let low = Object.values(historic[period])[2];
        let out = Object.values(historic[period])[1];
        let side;

        if (flgPosicao) {
            if (high['low'] == 'up' && high['cross']) {
            // if (asd == 'up' && high['cross']) {
                side = 'SELL';
                flgPosicao = false;
            }
        } else {
            if (low['high'] == 'up' && low['cross'] && out['high'] == 'up' /*&& out['cross']*/) {
                side = 'BUY';
                flgPosicao = true;
            }
        }
        if (historic[period]['strategy'] == undefined){
            historic[period]['strategy'] = {};
        }
        historic[period]['strategy'][strategy] = side;
    }
    for (key in historic) {
        historic[key] =  historic[key]['strategy'][strategy];
    }        
    return historic;
}

module.exports ={ crossValues, crossOrder };