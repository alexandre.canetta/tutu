/* eslint-disable consistent-return */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */

const axios = require('axios');
const CryptoJS = require('crypto-js');

const apiUrl = 'https://api.binance.com/api';

async function publicCall(
//   path: string,
  path,
  data = {},
  method = 'GET' /* , headers = {} */
) {
  try {
    let qs = "?";
    for (const key in data) {        
        qs = `${qs}${key}=${data[key]}&`
    };        
    const result = await axios({
      method,
      url: `${apiUrl}${path}${qs}`,
    });
    // return await result.data;
    return result.data;
  } catch (err) {
    console.error(err);
    // return public(path, data, method) //TESTE DE REENVIO
  }
}

module.exports = { publicCall };
