const { app, BrowserWindow, ipcMain } = require('electron');

//BINANCE DOCUMENTATION
//https://binance-docs.github.io/apidocs/spot/en/#introduction

let win;
// let apiKey;
// let secretKey;
let analysis;
let analysisId = analysis ? Object.keys(analysis).length : 0;
let inner;

const createWindow = () => {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
    autoHideMenuBar: true,
  } )

  win.loadFile('index.html')
}

//CHILD
// function createChildWindow(file, width, height) {
function createChildWindow(param) {
  childWindow = new BrowserWindow({
    width: param.width,
    height: param.height,
    modal: true,
    show: false,
    parent: win,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
    autoHideMenuBar: true,
  });
  
  childWindow.loadFile(param.file+".html");    
  
  childWindow.once("ready-to-show", () => {
    childWindow.show();
  });
}

// app.whenReady().then(() => {
//   createWindow()
// })

app.whenReady().then(() => {
  createWindow();  
  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});
  
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

ipcMain.on("openChildWindow", (event, param) => {
  createChildWindow(param);
});

// ipcMain.on('setKey', (event, key) => {
//   apiKey = key.apiKey;
//   secretKey = key.secretKey;
// })

// ipcMain.on('getKey', (event, title) => {
//   event.sender.send('clipboard-receive', apiKey)
// })


ipcMain.on('setAnalysis', (event, value) => { 
  analysis = {
    ...analysis,
    [analysisId] : value
  };
  // console.log(analysis);
  analysisId++;
});

ipcMain.on('getAnalysis', (event, value) => {    
  event.sender.send('analysis', analysis);  
});


ipcMain.on('setInner', (event, value) => {  
  inner = value;
  // win.reload();
  // console.log(inner);
});

ipcMain.on('getInner', (event, value) => {  
  event.sender.send('inner', inner);
})


ipcMain.on('refresh', (event, title) => {
  // console.log('refresh')
  // mainWindow.reload();
  win.reload();
})