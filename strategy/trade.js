async function get(historic = {}) {  
    let order;
    let trade = 0;
    let trades = {};
    
    for (const key in historic) {        
        order = `${historic[key]['strategy']['movingAverageCross']}`;
        switch (order) {
            case 'BUY':
                trade += 1;
                trades[trade] = {};
                trades[trade][order] = {};
                trades[trade][order]['startTime'] = key;
                trades[trade][order]['price'] = `${historic[key]['close']}`;
                break;
            case 'SELL':
                trades[trade][order] = {};
                trades[trade][order]['startTime'] = key;
                trades[trade][order]['price'] = `${historic[key]['close']}`;
                //ADICIONAR PRAZO, VARIACAO ABSOLUTA E VARIACAO RELATIVA, SE GANHO OU PERDA

                trades[trade]['absoluteVariation'] = trades[trade]['SELL']['price'] - trades[trade]['BUY']['price'];
                trades[trade]['relativeVariation'] = trades[trade]['SELL']['price'] / trades[trade]['BUY']['price'] - 1;
                trades[trade]['duration'] = trades[trade]['SELL']['startTime'] - trades[trade]['BUY']['startTime'];
                break;
        }       
        
    };
    // if (trades[trade]['SELL'] == undefined) delete trades[trade];
    return trades;
}

async function resume(trades = {}) {
    let result = {};
    let absoluteVariation = 0;

    result['trades'] = Object.keys(trades).length;
    result['winQty'] = 0;
    result['winVal'] = 0;
    result['loseQty'] = 0;
    result['loseVal'] = 0;
    result['drawQty'] = 0;
    result['drawVal'] = 0;
    result['absolute'] = 0;
    //TIRAR TRADE SEM VENDA

    for (const key in trades) {   
        // console.log()
        // if (`${trades[key]['SELL']}` != undefined) {
            // absoluteVariation = parseInt(`${trades[key]['absoluteVariation']}`);
            absoluteVariation = parseFloat(`${trades[key]['absoluteVariation']}`);
            // console.log(absoluteVariation)
            if (absoluteVariation > 0) {
                result['winQty'] += 1
                // result['winVal'] += absoluteVariation                
                console.log(result['winVal'])
                console.log(parseFloat(absoluteVariation.toFixed(8)))
                result['winVal'] = parseFloat((result['winVal'] + absoluteVariation).toFixed(8))
                // result['winVal'] += absoluteVariation
            };
            if (absoluteVariation < 0) {
                result['loseQty'] += 1
                // result['loseVal'] += absoluteVariation
                // result['loseVal'] += absoluteVariation
                result['loseVal'] = parseFloat((result['loseVal'] + absoluteVariation).toFixed(8))
            };
            if (absoluteVariation == 0) {
                result['drawQty'] += 1
                // result['drawVal'] += absoluteVariation
                // result['drawVal'] += parseabsoluteVariation
                result['drawVal'] = parseFloat((result['drawVal'] + absoluteVariation).toFixed(8))
            };
            result['absolute'] = parseFloat((result['absolute'] + absoluteVariation).toFixed(8))
            // result['absolute'] += absoluteVariation
        // }
    }
    console.log('result');    
    console.log(result);    
    return result;
}

module.exports = { get, resume };
